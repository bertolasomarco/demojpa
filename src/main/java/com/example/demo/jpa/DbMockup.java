package com.example.demo.jpa;

import com.example.demo.jpa.model.Author;
import com.example.demo.jpa.model.Book;
import com.example.demo.jpa.model.PublishingHouse;
import com.example.demo.jpa.repository.AuthorRepository;
import com.example.demo.jpa.repository.BookRepository;
import com.example.demo.jpa.repository.PublishingHouseRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Optional;

@Component
@RequiredArgsConstructor
public class DbMockup implements CommandLineRunner {

    private final AuthorRepository authorRepository;
    private final BookRepository bookRepository;
    private final PublishingHouseRepository houseRepository;

    @Override
    public void run(String... args) {

        final ArrayList<Author> authors = new ArrayList<>();
        final ArrayList<Book> books = new ArrayList<>();
        final ArrayList<PublishingHouse> publishingHouses = new ArrayList<>();

        final Author marco = new Author();
        marco.setFullName("Marco Pagan");
        marco.setBirthDate(LocalDate.of(1990, 5, 23));

        final Author giorgia = new Author();
        giorgia.setFullName("Giorgia Pagan");
        giorgia.setBirthDate(LocalDate.of(1994, 6, 1));

        final Book fuffa = new Book();
        fuffa.setPrice(5.30);
        fuffa.setReleaseDate(LocalDate.of(2020, 5, 4));
        fuffa.setTitle("Fuffa");
        final Book fuffa2 = new Book();
        fuffa2.setPrice(15.30);
        fuffa2.setReleaseDate(LocalDate.of(2000, 5, 12));
        fuffa2.setTitle("Fuffa2");

        final PublishingHouse corvallis = new PublishingHouse();
        corvallis.setName("Corvallis SRL");

        // LEGAMI
        final ArrayList<Book> libriDiMarco = new ArrayList<>();
        fuffa.setPublishingHouse(corvallis);
        fuffa.setAuthor(marco);
        libriDiMarco.add(fuffa);
        marco.setBooks(libriDiMarco);

        authors.add(marco);
        authors.add(giorgia);
        books.add(fuffa);
        books.add(fuffa2);
        publishingHouses.add(corvallis);

        houseRepository.saveAll(publishingHouses);
        authorRepository.saveAll(authors);
        bookRepository.saveAll(books);
    }
}
